import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {AdalService} from 'ng2-adal/dist/core';
import {DbService} from './services/db.service';
import {SecretService} from './services/secret.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {routes} from './routers/app.router'
import {HomeComponent} from "./components/home.component";
import {FaqComponent} from "./components/faq.component";
import {NoAccessComponent} from "./components/noaccess.component";
import {DocsComponent} from "./components/docs.component";
import {SanityComponent} from "./components/sanity.component";
import {UsageComponent} from "./components/usage.component";
import {HeaderComponent} from "./components/header.component";
import {FooterComponent} from "./components/footer.component";
import {EnvComponent} from "./components/env.component";
import {EnvTestComponent} from "./components/envtest.component";
import {KnowledgeDashBoard} from "./components/knowledgedb.component";
import {DowntimeComponent} from "./components/downtime.component";
import {WelcomeComponent} from "./components/welcome.component";
import {PostComponent} from "./components/post.component";
import {VideosComponent} from "./components/videos.component";
import {NotifyComponent} from "./components/notify.component";
import {LoggedInGuard} from "./authentication/logged-in.guard";


@NgModule({
  declarations: [
    AppComponent,HomeComponent,VideosComponent,WelcomeComponent,FaqComponent,NoAccessComponent,DocsComponent,SanityComponent,HeaderComponent,KnowledgeDashBoard,DowntimeComponent,UsageComponent,EnvComponent,PostComponent,EnvTestComponent,FooterComponent,NotifyComponent
  ],
  imports: [
    BrowserModule,routes,ReactiveFormsModule,HttpModule
  ],
  providers: [AdalService,SecretService,LoggedInGuard,DbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
