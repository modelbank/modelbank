import { Component } from '@angular/core';
@Component({
    selector: 'downtime',
    templateUrl:'./downtime.component.html'
  })

  export class DowntimeComponent {
    timeZoner:string;
    //pageTitle: string = 'Env';

    public getDate()
    {     
      var d = new Date();
      var utcMins = d.getUTCMinutes(); 
      var utcHrs = d.getUTCHours();
      console.log(d);
     // var serverTime = d.toTimeString();  
      var serverTime = d.toUTCString();
      console.log(serverTime);  
      return serverTime;
    }   
    public getMins()
    {     
      var d1 = new Date();
      var utcMins = d1.getUTCMinutes(); 
      console.log(utcMins);  
      return utcMins;
    }  

    public getHrs()
    {     
      var d2 = new Date();
      var utcHrs = d2.getUTCHours();
      console.log(utcHrs);  
      if (utcHrs==0)
      {
        return 24;
      }
      else if (utcHrs==1)
      {
        return 25;
      }
      else if (utcHrs==2)
      {  
        return 26;       
      }
      else
      {
        return utcHrs;
      }  
    }  
    public getSecs()
    {     
      var d2 = new Date();
      var utcSecs = d2.getUTCSeconds();
      console.log(utcSecs);  
      return utcSecs;
    } 
    
  }
    

//     var currenttime = '<? date_default_timezone_set('UTC'); print date("F d, Y H:i:s", time())?>' //PHP method of getting server date

//     ///////////Stop editing here/////////////////////////////////
    
//     var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
//     var serverdate=new Date(currenttime)
    
//     function padlength(what){
//     var output=(what.toString().length==1)? "0"+what : what
//     return output
//     }
    
//     function displaytime(){
      
//         var hours = serverdate.getHours();
//         var mid='AM';
//         if(hours==0){ //At 00 hours we need to show 12 am
//         hours=12;
//         }
//           else if(hours==12)
//         {
//         mid='PM';
//         }
      
//         else if(hours>12)
//         {
//         hours=hours%12;
//         mid='PM';
//         }
//     serverdate.setSeconds(serverdate.getSeconds()+1)
//     var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
//     var timestring=padlength(hours)+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())+ " "+mid
//     document.getElementById("servertime").innerHTML=datestring+" "+timestring
//     }
    
//     window.onload=function(){
//     setInterval("displaytime()", 1000)
//     }
//     // set the default timezone to use.
// date_default_timezone_set('UTC');

// $hour = date('G');

// if($hour >= 22 AND $hour < 3) {
//     header('location: https://modelbank.temenos.com/env/downtime.php');
// }
  
