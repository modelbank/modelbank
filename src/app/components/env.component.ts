import { Component } from '@angular/core';
import { Server } from 'selenium-webdriver/safari';


@Component({
    selector: 'env',
    templateUrl:'./env.component.html'
  })

  export class EnvComponent 
  {
    timeZoner:string;
   
    public getDate()
    {       
      var d = new Date();
      var serverTime = d.toUTCString();
         
      return serverTime;
    } 
       
    public getVersionURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     
     
     var dynamicVersionURL = "https://modelbank"+DMY+".westeurope.cloudapp.azure.com/manifest.html"
     // http://modelbank.westeurope.cloudapp.azure.com/manifest.html
  
     return dynamicVersionURL;
   } 

    public getBrowserURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     
     
     var dynamicBrowserURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/BrowserWeb"; 
  
     return dynamicBrowserURL;
   } 
   public getUXPURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     
     
     var dynamicUXPURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/Browser"; 
  
     return dynamicUXPURL;
   } 
   public getRetailURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     
     
     var dynamicRetailURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/Retail"; 
  
     return dynamicRetailURL;
   } 
   public getCorporateURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
    
     
     var dynamicCorporateURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/Corporate"; 
  
     return dynamicCorporateURL;
   }
   public getTFOURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     
     
     var dynamicTFOURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/TFO"; 
  
     return dynamicTFOURL;
   }
  }
    

