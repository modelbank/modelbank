import { Component } from '@angular/core';
import { Server } from 'selenium-webdriver/safari';


@Component({
    selector: 'env',
    templateUrl:'./envtest.component.html'
  })

  export class EnvTestComponent 
  {
    timeZoner:string;
   
    public getDate()
    {       
      var d = new Date();
      var serverTime = d.toUTCString();
      console.log(serverTime);     
      return serverTime;
    }    
    public getURL()
   {      
    var nd = new Date(); 
    var currentUTCDay = nd.getUTCDate();
    var currentUTCMonth = nd.getUTCMonth()+1;
    var currentUTCYear = nd.getUTCFullYear();
    var str = new String(currentUTCYear);
   
    var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
    console.log(DMY);
    
    var dynamicURL = "http://modelbank"+DMY+".westeurope.cloudapp.azure.com:9089/Corporate"; 
 
    return dynamicURL;
  } 
  }
    

