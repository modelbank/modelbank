import { Component } from '@angular/core';
import { AdalService } from 'ng2-adal/dist/core';
import {Router} from "@angular/router";
@Component({
  selector: 'app-header',
  templateUrl:'./header.component.html'
})
export class HeaderComponent {
 loginStatus:boolean=false;
  userName:string;
 showNotify:boolean;
  // sessionStorage.setItem("flag", "false");
  constructor(
    private adalService: AdalService,
    private router: Router
  ) {
   
    console.log('user', this.adalService);
    if(this.adalService.userInfo.isAuthenticated){
      this.loginStatus=true;
    this.userName= this.adalService.userInfo.profile.family_name;
    }
        }
      // chatToggle(){

      // }  
public logOut() {
  this.adalService.logOut();
}
}



