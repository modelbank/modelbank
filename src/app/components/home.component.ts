import { Component, OnInit } from '@angular/core';
import { AdalService } from 'ng2-adal/dist/core';
import {Router, NavigationStart} from "@angular/router";
// import * as $ from "jquery";
declare var $: any;
@Component({
  selector: 'home',
  templateUrl:'./home.component.html'
})

export class HomeComponent implements OnInit {
  networkLink: string;
  downtimeLink: string;
  networkCheck:boolean=false;
  modalLoadCheck:boolean=false;
  static modalLoadCounter=0;
  userName:string;
  constructor(
    private adalService: AdalService,
    private router: Router)     
        {
          this.userName= this.adalService.userInfo.profile.family_name;
        }
        ngOnInit() 
        {
          if(HomeComponent.modalLoadCounter<1)
          {
            $("#test").trigger("click");
            console.log("In if = "+HomeComponent.modalLoadCounter);
            HomeComponent.modalLoadCounter++;
          }
          else
          {
            console.log("Modal loaded once already.");
            console.log("In else = "+HomeComponent.modalLoadCounter);
          }           
        }

        networkTest(){
          
          let ImageObject = new Image();
          ImageObject.src = "http://maa1mbwin1:1111/icons/blank.gif";
          if(ImageObject.height==0){
           this.networkCheck=false;    
           this.networkLink="/#/sanity";
            
          }
          else{
            this.networkCheck=true;
            this.networkLink="/#/sanity"
          }      
        }

    public checkDowntime()
    {
      var downTimeDate = new Date();    
      var utcHours = downTimeDate.getUTCHours();
      console.log(utcHours);
     //  var utcMinutes = downTimeDate.getUTCMinutes();         
     // console.log(utcMinutes);
     if(utcHours>=22 || utcHours<=2)
    //if(utcHours>=3 || utcHours<=7)
        {                      
           this.downtimeLink="/#/downtime";
           return this.downtimeLink;
         }
      else
          {           
           this.downtimeLink="/#/env";
           return this.downtimeLink;
         }          
    }

    public getSanityURL()
    {      
     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);
    
     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     console.log(DMY);
     
     var dynamicSanityURL = "https://modelbank"+DMY+".westeurope.cloudapp.azure.com/Report/IndexMaster.html"
     // https://modelbank6618.westeurope.cloudapp.azure.com/Report/IndexMaster.html
  
     return dynamicSanityURL;
   } 

public logOut() {
  this.adalService.logOut();
}
}