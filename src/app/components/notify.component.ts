import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {DbService} from '../services/db.service';
@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html'
})
export class NotifyComponent implements OnInit {
  constructor(private db:DbService) { }
  notifyData:Array<{}>;
  ngOnInit() {
    this.db.retriveData().subscribe((res:Array<{}>)=>{
        if(res){
            console.log(res);
            this.notifyData = res;
        }
    })
  }

}
