import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {DbService} from '../services/db.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html'
})
export class PostComponent implements OnInit {

title = 'app';

  successMsg:boolean;
  notifyForm:FormGroup;
  constructor(private fb: FormBuilder,private router: Router,private db:DbService) { }

  ngOnInit() {
    this.notifyForm = this.fb.group({
      "title":['',[Validators.required]],
      "desc":['',[Validators.required]],
     // "id":['',[Validators.required]]
      "date":['',[Validators.required]],
    })
   // $("#test").trigger("click");
    
  }

submitData(){
  
let data ={
  title: this.notifyForm.get('title').value,
  description: this.notifyForm.get('desc').value,
  // id: this.notifyForm.get('id').value
  date: this.notifyForm.get('date').value
}
console.log(data);
this.db.sendData(data).subscribe(res=>{
  if(res===201){
console.log(res);
  }
})
}
  delData(){
    this.db.deleteData().subscribe(res=>{
      if(res===201){
    console.log(res);
      }
    })   
      }

}
