 
// import { Component } from '@angular/core';
// @Component({
//     selector: 'sanity',
//     templateUrl:'./sanity.component.html'
//   })

//   export class SanityComponent {

//     public getSanURL()
//     {      
//      var nd = new Date(); 
//      var currentUTCDay = nd.getUTCDate();
//      var currentUTCMonth = nd.getUTCMonth()+1;
//      var currentUTCYear = nd.getUTCFullYear();
//      var str = new String(currentUTCYear);
    
//      var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
//      console.log(DMY);
     
//      var dynamicSanURL = "https://modelbank"+DMY+".westeurope.cloudapp.azure.com/Report/IndexMaster.html"; 
  
//      return dynamicSanURL;
//    }
//    } 

import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Component } from '@angular/core';

@Component({
    selector: 'sanity',
    templateUrl:'./sanity.component.html'
  })

  export class SanityComponent {
    dynamicSanURL:any;
    constructor(private sanitizer: DomSanitizer)
      {
         
      }
  safeUrl: SafeResourceUrl;
  ngOnInit() {


     var nd = new Date(); 
     var currentUTCDay = nd.getUTCDate();
     var currentUTCMonth = nd.getUTCMonth()+1;
     var currentUTCYear = nd.getUTCFullYear();
     var str = new String(currentUTCYear);

     var DMY = currentUTCDay.toString()+currentUTCMonth.toString()+(str.charAt(2)).toString()+(str.charAt(3)).toString();
     console.log(DMY);
     

     this.dynamicSanURL = this.sanitizer.bypassSecurityTrustResourceUrl("https://modelbank"+DMY+".westeurope.cloudapp.azure.com/Report/IndexMaster.html");
     this.safeUrl = `this.sanitizer.bypassSecurityTrustResourceUrl(dynamicSanURL);`
     
  }
   } 

  

