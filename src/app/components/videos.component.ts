import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {DbService} from '../services/db.service';
import * as $ from 'jquery';

@Component({
  //selector: 'app-post',
  templateUrl: './videos.component.html'
})
export class VideosComponent implements OnInit {

title = 'app';
  notifyForm:FormGroup;
  constructor(private fb: FormBuilder,private router: Router,private db:DbService) { }

  ngOnInit() {
    this.notifyForm = this.fb.group({
      "title":['',[Validators.required]],
      "desc":['',[Validators.required]],
      "date":['',[Validators.required]],
    })
   
    
  }
  }

 


