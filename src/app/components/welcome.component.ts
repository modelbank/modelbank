import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AdalService} from 'ng2-adal/dist/core';
/*
@Component({
  selector: 'welcome',
  template: `
  <div style="text-align:center;margin-top:40vh;" class="container">
  <div>
  <h1>Welcome to Model Bank</h1>
  </div>
  <div>
  <a class="" (click)="logIn()"><img id="login-logo" title="Click to proceed" src="assets/images/blacktem.png"/></a>
  </div>
  </div>
  `
}) */

@Component({
  selector: 'welcome',
  template: `
  <div style="text-align:center;margin-top:40vh;" class="container">
  <div>
  <h1>Welcome to Model Bank</h1>
  </div>
  <div>
  <a class="temenos-icon" title="Click to proceed" (click)="logIn()"> </a>
  </div>
  </div>
  `
})

export class WelcomeComponent implements OnInit {

  constructor(
    private router: Router,
    private adalService: AdalService
  ) {
    console.log('Entering welcome', this.adalService.userInfo);

    if (this.adalService.userInfo.isAuthenticated) {
				console.log(location.hash);
      // this.router.navigate(['/#/home']);
    }
  }

  public ngOnInit() {
  }

  public logIn() {
    this.adalService.login();
  }
}
