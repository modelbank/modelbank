import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "../components/home.component";
import {WelcomeComponent} from "../components/welcome.component";
import {LoggedInGuard} from "../authentication/logged-in.guard";
import {FaqComponent} from "../components/faq.component";
import {NoAccessComponent} from "../components/noaccess.component";
import {DocsComponent} from "../components/docs.component";
import {UsageComponent} from "../components/usage.component";
import {EnvComponent} from "../components/env.component";
import {EnvTestComponent} from "../components/envtest.component";
import {DowntimeComponent} from "../components/downtime.component";
import {SanityComponent} from "../components/sanity.component";
import {KnowledgeDashBoard} from "../components/knowledgedb.component";
import {PostComponent} from "../components/post.component";
import {VideosComponent} from "../components/videos.component";

export const router: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'faq',
    component:FaqComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'videos',
    component:VideosComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'usage',
    component:UsageComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'noaccess',
    component:NoAccessComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'env',
    component:EnvComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'envtest',
    component:EnvTestComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'knowledgedb',
    component:KnowledgeDashBoard,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'downtime',
    component:DowntimeComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'docs',
    component:DocsComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'sanity',
    component:SanityComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'post',
    component:PostComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  { path: '**', redirectTo: 'home' }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router,{useHash: true});
