import {Injectable} from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';


 // const url = "https://private-f4e7c9-tarunsinghbisht.apiary-mock.com/";

// const url = "http://localhost:8080/";

 const url = "https://temenosbnkinfra.cloudapp.net:443/";

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'text/plain' })
};
 
@Injectable()
export class DbService {
 
    constructor(private http:Http) {}
 
    // Uses http.get() to load data from a single API endpoint
    sendData(data):Observable<{}> {
        return this.http.put(url+'insert.php?date='+data.date+'&title='+data.title+'&description='+data.description, data, {
            headers: new Headers({ 'Content-Type': 'application/json' })
        }).map(response=>{
            const res = response;
            return res;
        })
    }

    retriveData():Observable<{}>{
        return this.http.get(url+'retrieve.php',httpOptions).map((res:Response)=>{
            return res.json()});
    }

    
    deleteData():Observable<{}>{
        return this.http.get(url+'delete.php',httpOptions)
            //headers: new Headers({ 'Content-Type': 'text/plain'});
    }
}