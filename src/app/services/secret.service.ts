import {Injectable} from '@angular/core';

// Application specific configuration 
@Injectable()
export class SecretService {
  public get adalConfig(): any {
    return {
      tenant: 'temenosgroup.onmicrosoft.com',
      clientId: 'ed712912-7ac7-46a4-8f5f-6164392c3383',
      redirectUri: window.location.origin + '/#/home',
      postLogoutRedirectUri: window.location.origin + '/'
    };
  }
}
